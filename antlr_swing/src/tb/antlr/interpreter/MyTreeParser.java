package tb.antlr.interpreter;

import org.antlr.runtime.*;
import org.antlr.runtime.tree.*;
import tb.antlr.symbolTable.*;

public class MyTreeParser extends TreeParser {

	protected GlobalSymbols globalSymbols = new GlobalSymbols();
	
    public MyTreeParser(TreeNodeStream input) {
        super(input);
    }

    public MyTreeParser(TreeNodeStream input, RecognizerSharedState state) {
        super(input, state);
    }

    protected void drukuj(String text) {
        System.out.println(text.replace('\r', ' ').replace('\n', ' '));
    }

	protected Integer getInt(String text) {
		return Integer.parseInt(text);
	}
	
	// funckja do silnii
	protected Integer fractorial(Integer a) {
		int result = 1;
		for(int i = a; i > 0 ; i--) {
			result *= i;
		}
		return result;
	}
	
	// funkcja do potegowania
	protected Integer power(Integer a, Integer b) {
		int result = 1;
		for(int i = b; i > 0 ; i--) {
			result *= a;
		}
		return result;
	}
	
	protected void newVar(String x) {

		globalSymbols.newSymbol(x);

	}	

	protected Integer setVar(String x, Integer a) {

		globalSymbols.setSymbol(x,a);

		return globalSymbols.getSymbol(x);

	}

	protected Integer getVar(String x) {

		return globalSymbols.getSymbol(x);

	}
	
}
