grammar Expr;

options {
  output=AST;
  ASTLabelType=CommonTree;
}

@header {
package tb.antlr;
}

@lexer::header {
package tb.antlr;
}

prog
    : (stat )+ EOF!;

stat
    : expr NL -> expr
+   | PRINT expr -> ^(PRINT expr)

//    | VAR ID PODST expr NL -> ^(VAR ID) ^(PODST ID expr)
    | VAR ID NL -> ^(VAR ID)
    | ID PODST expr NL -> ^(PODST ID expr)

    | NL ->
    ;

expr
    : multExpr
      ( PLUS^ multExpr
      | MINUS^ multExpr
      )*
    ;

multExpr
    : siln
      ( MUL^ siln
      | DIV^ siln
      )*
    ;

// zdefiniowanie zachowania dla silnii
siln
    : pot
    (SILNIA^)*
    ;
    
// zdefiniowanie zachowania dla potęgi  
pot
    : atom
    ( POW^ pot   
    )?
    ;

atom
    : INT
    | ID
    | LP! expr RP!
    ;

VAR :'var';

PRINT: 'print';


ID : ('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'0'..'9'|'_')*;

INT : '0'..'9'+;


NL : '\r'? '\n' ;

WS : (' ' | '\t')+ {$channel = HIDDEN;} ;


LP
	:	'('
	;

RP
	:	')'
	;

PODST
	:	'='
	;

PLUS
	:	'+'
	;

MINUS
	:	'-'
	;

MUL
	:	'*'
	;

DIV
	:	'/'
	;
	
POW
  : '^'
  ;

SILNIA
  : '!'
  ;